<?php
/*********************************************
Files Includes
**********************************************/
	require_once 'function.php';
	header('Content-type: text/html; charset=utf-8');
	session_start();
	
	/*
	 * $page_list is array who contane data about avry page in Admin-Site
	 * all url nevigate to index.php page.
	 * and thit page check if requsted page exist and get the appropriate content.
	 *
	 */
	$_PARAMS = array(
		"electionImage"   => "http://electit.elhay.co.il/images/electionImage/",
		"contestantImage"   => "http://electit.elhay.co.il/images/contestantImage/",
		"userImage" => "http://electit.elhay.co.il/images/userImage/"
		);
	$page_list = array(
		    'home' => array(
		    	'isPrivate' => false,
		        'title' => 'Home Page',
		        'content' => 'public/home.php',
		        'scripts' => array( 'main' , 'public', 
		        	'libs' => array('jquery', 
		        		'jquery.validate', 
		        		'bootstrap'
		        		) 
		        	),
		        'css' => array( 'style', 
		        	'libs' => array( 'bootstrap', 
		        		'font-awesome' 
		        		)
		        	),
		        'message' => array('public/about/about','public/contactUs/contactUs', 'public/login/login', 'public/registration/registration' )
		    ),
		    'dashboard' => array(
		    	'isPrivate' => true,
		        'title' => 'Manage dashboard',
		        'content' => 'private/dashboard/dashboard-page.php',
		        'scripts' => array( 
		        	'main', 
		        	'dashboard', 
		        	'libs' => array('jquery', 
		        		'jquery.validate', 
		        		'bootstrap',
		        		'bootstrap-datetimepicker'
		        		) 
		        	),
		        'css' => array( 'style', 'dashboard', 
		        	'libs' => array( 'bootstrap', 
		        		'font-awesome', 
		        		'bootstrap-datetimepicker' 
		        		)
		        	),		        
		        'message' => array(
		        	'public/about/about', 
		        	'public/contactUs/contactUs',
		        	'private/messages/election/edit', 
		        	'private/messages/election/delete' )
		    ),
		    'election' => array(
		    	'isPrivate' => true,
		        'title' => 'Edit Election',
		        'content' => 'private/election/election-page.php',
		        'scripts' => array( 
		        	'main', 
		        	'dashboard', 
		        	'election',
		        	'libs' => array('jquery', 
		        		'jquery.validate', 
		        		'bootstrap',
		        		'bootstrap-datetimepicker',
		        		'vendor/jquery.ui.widget',
		        		'jquery.iframe-transport',
		        		'jquery.fileupload',
		        		// 'jquery.fileupload-jquery-ui',
		        		// 'jquery.fileupload-process',
		        		// 'jquery.fileupload-ui',
		        		// 'jquery.fileupload-image'
		        		) 
		        	),
		        'css' => array( 'style', 'dashboard', 'election',
		        	'libs' => array( 'bootstrap', 
		        		'font-awesome' ,
		        		'bootstrap-datetimepicker',
		        		'jquery.fileupload',
		        		'jquery.fileupload-ui'
		        		)
		        	),		        
		        'message' => array(
		        	'public/about/about', 
		        	'public/contactUs/contactUs',
		        	'private/messages/contestant/edit', 
		        	'private/messages/contestant/delete',
		        	'private/messages/election/edit', 
		        	'private/messages/election/delete' )
		    ),
		    '404' => array(
				'isPrivate' => false,		    		
				'title' => "Page not found",
				'content' => 'public/404.php',
				'scripts' => array( 'main' , 'public',
		        	'libs' => array('jquery', 
		        		'jquery.validate', 
		        		'bootstrap'
		        		) 
		        	),
		        'css' => array( 'style', 
		        	'libs' => array( 'bootstrap', 
		        		'font-awesome' 
		        		)
		        	),
		        'message' => array('public/about/about', 'public/login/login', 'public/registration/registration' )
		    ),
		    'access-denied' => array(
				'isPrivate' => false,		    		
				'title' => "Access to this Page denied",
				'content' => 'public/access-denied.php',
				'scripts' => array( 'main' , 'public',
		        	'libs' => array('jquery', 
		        		'jquery.validate', 
		        		'bootstrap'
		        		) 
		        	),
		        'css' => array( 'style', 
		        	'libs' => array( 'bootstrap', 
		        		'font-awesome' 
		        		)
		        	),
		        'message' => array('public/about/about', 'public/login/login', 'public/registration/registration' )
		    )

		);

	// get page from query  or go to home page
	$page  = ( isset( $_GET['page'] ) )? $_GET['page'] : 'home' ;

	// check if page name is valid name of exist page in $page_list array
	$page_info = ( array_key_exists( $page, $page_list ) ) ? $page_list[$page]: $page_list['404'];


    if( ! isLogin() ){

		if( $page_info['isPrivate'] ){

			// access denied
			$page_info = $page_list['access-denied'];
		}

	}else{

		// if no page var in $_GET go to home page for user
		$page_info = ( $page == 'home' ) ? $page_list['dashboard'] : $page_info ;

	}
	    
	// init page title
	$the_title = $page_info['title'];

	// call to enqueStyles 
	$_resurse->styles->enque( $page_info['css'] );
	// call to enqueScripts 
	$_resurse->scripts->enque( $page_info['scripts'] );
	// call to add message function
	$_resurse->messages->enque( $page_info['message'] );

	require_once('header.php');
	
	// print page content
	require_once( $page_info['content'] );

	require_once 'footer.php';
	

?>