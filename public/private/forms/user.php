<form role="form" id="edit-user-form">
	<table class="table table-striped">
		<tr>
			<th>
				name
			</th>
			<td>
				<div id="firstName" class="user-form" contenteditable="false"><?php echo $_SESSION['firstName']; ?></div>
			</td>
		</tr>
		<tr>
			<th>
				last name
			</th>
			<td>
				<div id="lastName" class="user-form" contenteditable="false"><?php echo $_SESSION['lastName']; ?></div>
			</td>
		</tr>
		<tr>
			<th>
				Email
			</th>
			<td>
				<div id="eMail" class="user-form" contenteditable="false"><?php echo $_SESSION['eMail']; ?></div>
			</td>
		</tr>
	</table>
	<a class="btn btn-primary" id="edit-button" onclick="ElectIt.dashboard.user.editable()">Edit your details</a>
	<button type="submit" class="btn btn-success edit-mode hide" >Send</button>
	<a class="btn btn-warning edit-mode hide" onclick="ElectIt.dashboard.user.cancel()">Cancel</a>
	<a class="btn btn-danger edit-mode hide" onclick="ElectIt.dashboard.user.showDeleteModal()">Delete account</a>
</form>
<?php include "private/messages/user/delete.html"; ?>