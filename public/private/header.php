<?php 
	if(!isset($active_home)) $active_home = "";
	if(!isset($active_about)) $active_about = "";
	if(!isset($active_contactus)) $active_contactus = "";
	if(!isset($active_login)) $active_login = "";
	if(!isset($active_registration)) $active_registration = "";
 ?>


<div class="container page-header">
	<div class="header">
			<ul class="nav nav-pills pull-right">
				<li class="<?php echo $active_home; ?>"><a href="index.php?page=dashboard">Dashboard</a></li>
				<li class="<?php echo $active_about; ?>"><a href="#" class="about-php">About</a></li>
				<li class="<?php echo $active_contactus; ?>"><a href="#" class="contactUs">Contact Us</a></li>
				<li class="<?php echo $active_login; ?>"><a href="?logout=true" class="logout-button">Log out</a></li>
			</ul>
		<div class="logo"></div>
		<div class="text-muted"><h3>Elect - it</h3></div>
	</div>
	</div>
<!-- End page HEAD -->
<div class="container">
	<div class="row">
		<div id="alert"></div>
	</div>
</div>