
<div class="row">
	<div class="panel  panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Personal Detailes</h3>
		</div>
		<div class="panel-body">
			<?php include "private/forms/user.php"; ?>
		</div>
	</div>
	<div class="panel  panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Add new Election</h3>
		</div>
		<div class="panel-body">

			<div class="new-election">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" id="election-form-tabs">
				  <li><a href="#step-1" data-toggle="tab"><strong>1</strong> Details</a></li>
				  <li><a href="#step-2" data-toggle="tab"><strong>2</strong> Contestants</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
				  <div class="tab-pane active" id="home">...</div>
				  <div class="tab-pane" id="step-1">
					<?php include 'private/forms/new-election/step-1.html'; ?>
				  </div>
				  <div class="tab-pane" id="step-2">
					<?php include 'private/forms/new-election/step-2.html'; ?>
				  </div>
				</div>				
				<div id="form-footer">
					<a class="btn btn-success" id="new-election-send" >next</a>
				</div> 

			</div>
			
		</div>
	</div>
</div>

