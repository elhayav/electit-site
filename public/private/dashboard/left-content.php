<div class="panel  panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Election List</h3>
	</div>
	<div class="panel-body">
	<?php 
		global $_PARAMS;
		if( $results && $results->num_rows ){
			
			while( $election = mysqli_fetch_assoc( $results ) ){ ?>
			<?php 
				if($election['electionImage'] == null || $election['electionImage'] == 'null' || $election['electionImage'] == ''){
					$election['electionImage'] = 'default.png';
				}
			?>
		<div class="row row-hover row-padding">
			<div class="col-md-2">
				<img src="<?php echo $_PARAMS['electionImage'],$election['electionImage']; ?>" alt="" class="img-thumbnail img-responsive">
			</div>
			<div class="col-md-7">
				<a href="?page=election&electionID=<?php echo $election['ID']; ?>">
					<h4>election no'<?php echo $election['ID']; ?> - <?php echo $election['electionName']; ?></h4>
				</a> 
				<p>this election ends at <?php echo $election['endElectionDate']; ?></p>
			</div>
			<div class="col-md-3 election-row-btns">
				<a class="btn btn-primary" action="edit" election-id="<?php echo $election['ID'] ?>">Edit</a>
				<a class="btn btn-danger" action="delete" election-id="<?php echo $election['ID'] ?>">Delete</a>
			</div>
		</div>
	<?php }
		}else{ ?>
		
		<div class="row row-padding">
			<div class="col-md-12">
				<div class="well">You dont create election yet ... <a href="">Create a new election</a> </div>
			</div>
		</div>

		<?php }

	 ?>
	</div>
</div>
