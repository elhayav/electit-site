<?php 
/*********************************************
	Page variabels
 **********************************************/
	$active_home = "active";

	include 'private/header.php';

	global $table_election;

	/* get user data from DB */
	$results = exeQuery('SELECT * FROM '. $table_election->table_name .' WHERE manager='. $_SESSION['userId']);
	?>
<div class="container">
	
	<div class="row">
		<div class="col-md-8">
			<?php include "left-content.php"; ?>
		</div>
		<div class="col-md-4">
			<?php include "right-content.php"; ?>
		</div>
	</div>
	
</div>