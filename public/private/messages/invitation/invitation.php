<div class="alert alert-warning alert-dismissable invite-users-alert">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>Invite people</strong> invite people to vote in your new Election.
	<p>
		To register for your election users need to enter in there Android App
		this election ID: <strong><?php echo ($election['ID'])? $election['ID']: 'ops no election id founded..'; ?></strong>
	</p>
	<div class="hover-show">
		<h2>Send invitation</h2>
		<p>
			You can add here list of emails to invite them to register to you election,<br>
			and we automatically:<br>
			- Register them to this election.<br>
			- sen them email with your details and this election details.<br>
		</p>
		<div class="row invitation">
			<div class="col-md-6">
				<form role="form" id="invite-user-form">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						<input type="email" class="form-control" name="email" id="email" placeholder="type Email address and press Enter" autofocus required>
					</div>
				</form>
			</div>
			<div class="col-md-6">
				<form role="form" id="invite-users-form">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						<textarea rows="4" class="form-control" name="emailsList" id="emails-list" placeholder="Type list of emails seperated with coma and press Enter" ></textarea>
					</div>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h3>Your invitation will be send to this address:</h3>
				<div class="well">
					<ul class="temp-users-list">
					</ul>
				</div>
			</div>
		</div>
		<a class="btn btn-primary" onclick="ElectIt.dashboard.invitations.send(<?php echo $election['ID']; ?>)">Send Invitations</a>
	</div>
</div>