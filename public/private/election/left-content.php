	<?php 
		global $_PARAMS;
		if( $results && $results->num_rows ){

		$election = mysqli_fetch_assoc( $results );

		if( $election['ID']){
			$results = exeQuery('SELECT * FROM '. $table_contestant->table_name .' WHERE election='. $election['ID']);
		}
	?>
		<div class="row">
			<div class="col-md-12">
				<?php include"private/messages/invitation/invitation.php"; ?>
			</div>
		</div>
		<div class="row well election-details">
			<?php
				if($election['electionImage'] == null || $election['electionImage'] == 'null' || $election['electionImage'] == ''){
					$election['electionImage'] = 'default.png';
				}
			?>
			<div class="col-md-2">
				<img src="<?php echo $_PARAMS['electionImage'],$election['electionImage']; ?>" alt="" class="img-thumbnail img-responsive">
			</div>
			<div class="col-md-7">
				<h4>election no'<?php echo $election['ID']; ?> - <?php echo $election['electionName']; ?></h4>
				<p>this election ends at <?php echo $election['endElectionDate']; ?></p>
				<p>
					<?php $election['description']; ?>
				</p>
			</div>

			<div class="col-md-3 election-row-btns">
				<a class="btn btn-primary" action="edit" election-id="<?php echo $election['ID'] ?>">Edit</a>
				<a class="btn btn-danger" action="delete" election-id="<?php echo $election['ID'] ?>">Delete</a>
				<span class="btn btn-success fileinput-button">
                    <i class="fa fa-arrow-circle-o-up"></i>
                    <span>Upload Image</span>
                    <input id="fileupload-election" type="file" name="files[]" data-url="api/election.php" election-id="<?php echo $election['ID'] ?>" multiple>
                </span>
			</div>
			<!-- Election results -->
			<?php

				$contestants = $results;
				$sumVotes = 0;
				$maxVotes = 0;
				while( $contestant = mysqli_fetch_assoc( $contestants )) {

					$sumVotes += $contestant['votesCount'];

					if( intval($contestant['votesCount']) >= $maxVotes ){
						$maxVotes = intval($contestant['votesCount']);
					}
				}
				mysqli_data_seek($results,0);
			 ?>
		</div>
	<?php }else{ ?>

		<div class="row row-padding">
			<div class="col-md-12">
				<div class="well">No election match to this ID </div>
			</div>
		</div>

		<?php }

	 ?>

 <!-- Contestants -->
<div class="panel  panel-primary panel-contestants">
	<div class="panel-heading">
		<h3 class="panel-title">Contestant List</h3>
	</div>
	<div class="panel-body">
		<?php



		if( $results && $results->num_rows ){

			while( $contestant = mysqli_fetch_assoc( $results ) ){ ?>
			<?php
				if($contestant['contestantImage'] == null || $contestant['contestantImage'] == 'null' || $contestant['contestantImage'] == ''){
					$contestant['contestantImage'] = 'default.png';
				}
			?>
		<div class="row row-hover row-padding <?php echo ( intval($contestant['votesCount']) >= $maxVotes )? 'leadingContestant':''; ?>">
			<div class="col-md-2">
				<img src="<?php echo $_PARAMS['contestantImage'],$contestant['contestantImage']; ?>" alt="" contestant-id="<?php echo $contestant['ID'] ?>" class="img-thumbnail img-responsive">
			</div>
			<div class="col-md-7">
				<a onClick="ElectIt.electionView.contestantList.showEditContestantModal(<?php echo $contestant['ID'] ?>, <?php echo $contestant['election'] ?>)">
					<h4>contestant no'<?php echo $contestant['ID']; ?> - <?php echo $contestant['fullName']; ?></h4>
				</a>
				<div class="progress progress-striped active">
					<div class="progress-bar progress-bar-<?php echo ( intval($contestant['votesCount']) >= $maxVotes )? 'success':'warning'; ?>"  role="progressbar" aria-valuenow="<?php echo $contestant['votesCount']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $sumVotes; ?>" style="width: <?php echo (( 100 / $sumVotes ) *$contestant['votesCount']); ?>%;">
						<span class="sr-only"><?php echo $contestant['votesCount']; ?> Votes</span>
					</div>
				</div>
				<p>number of Votes <?php echo $contestant['votesCount']; ?></p>
				<p>
					<?php echo $contestant['description']; ?>
				</p>
			</div>
			<div class="col-md-3 election-row-btns">
				<a class="btn btn-primary" onClick="ElectIt.electionView.contestantList.showEditContestantModal(<?php echo $contestant['ID'] ?>, <?php echo $contestant['election'] ?>)">Edit</a>
				<a class="btn btn-danger" onClick="ElectIt.electionView.contestantList.showDeleteContestantModal(<?php echo $contestant['ID'] ?>, <?php echo $contestant['election'] ?>)">Delete</a>
				<span class="btn btn-success fileinput-button">
                    <i class="fa fa-arrow-circle-o-up"></i>
                    <span>Upload Image</span>
                    <input class="fileupload-contestant" type="file" name="files[]" data-url="api/contestant.php" contestant-id="<?php echo $contestant['ID'] ?>" multiple>
                </span>
			</div>
		</div>
	<?php }
		}else{ ?>

		<div class="row row-padding">
			<div class="col-md-12">
				<div class="well">You dont create any Contestants yet ... <a href="#">Create a new contestant</a> </div>
			</div>
		</div>

		<?php }

	 ?>
	</div>
</div>

<!-- Users -->
<div class="panel  panel-primary panel-users">
	<div class="panel-heading">
		<h3 class="panel-title">Voters List</h3>
	</div>
	<div class="panel-body">
	<?php

		if( $election['ID'] ){
			$results = exeQuery('SELECT
									'.$table_users->table_name.'.*,
									'.$table_election_to_users->table_name.'.*
								FROM
									'.$table_users->table_name.'
								LEFT JOIN '.$table_election_to_users->table_name.'
								ON '.$table_users->table_name.'.ID = '.$table_election_to_users->table_name.'.userID
								WHERE '.$table_election_to_users->table_name.'.electionID = '.$election['ID']);
		}
		if( $results && $results->num_rows ){

			while( $voters = mysqli_fetch_assoc( $results ) ){ ?>
			<?php
				if($voters['userProfileImg'] == null || $voters['userProfileImg'] == 'null' || $voters['userProfileImg'] == ''){
					$voters['userProfileImg'] = 'default.png';
				}
			?>
		<div class="row row-hover row-padding">
			<div class="col-md-2">
				<img src="<?php echo $_PARAMS['userImage'],$voters['userProfileImg']; ?>" alt="" class="img-thumbnail img-responsive">
			</div>
			<div class="col-md-6">
				<h4><?php echo $voters['firstName']; ?> <?php echo ($voters['lastName'] != NULL && $voters['lastName'] != 'null')?$voters['lastName']: ''; ?></h4>
				<p>
				<?php
					echo ($voters['voteTo'] != NULL && $voters['voteTo'] != 'null')?'<span class="label label-success">Vote</span>': '<span class="label label-default">Not vote</span>';
					echo ($voters['manageApprove'] == 'waiting')? ' <span class="label label-info">NEW VOTER</span>':'';
				?>
					<?php echo '<a href="mailto:'.$voters['eMail'].'">'.$voters['eMail'].'</a>'; ?>
				</p>
			</div>
			<div class="col-md-4 election-row-btns">
			<?php
				if($voters['manageApprove'] == 'waiting'){
					echo '<a class="btn btn-success" onClick="ElectIt.electionView.voters.approved('.$voters['ID'].')">Approved</a>';
				}else if($voters['manageApprove'] == 'true'){
					echo '<a class="btn btn-danger" onClick="ElectIt.electionView.voters.remove('.$voters['ID'].')">Remove</a>';
				}else if($voters['manageApprove'] == 'false'){
					echo '<a class="btn btn-info" onClick="ElectIt.electionView.voters.approved('.$voters['ID'].')">Return</a>';
				}
			 ?>
			</div>
		</div>
	<?php }
		}else{ ?>

		<div class="row row-padding">
			<div class="col-md-12">
				<div class="well">You dont have any Voters yet ... <a href="#">Invite voters</a> </div>
			</div>
		</div>

		<?php }

	 ?>
	</div>
</div>
