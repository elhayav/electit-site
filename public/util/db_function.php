<?php 
/*********************************************
	Files Includes
	**********************************************/
	require_once 'dal.php';

	function exeQuery( $query='' ){

		$resualt = '';//INSERT resualt
		$con = connect_to_db();//connection link

		if(mysqli_connect_errno($con))
		{
			//ERROR message when connection failed
			// echo("Failed to connect to MySQL: " . mysqli_connect_error());
		}
		else
		{
			mysqli_set_charset($con, "utf8");
			$resualt = mysqli_query($con, $query);
			
			mysqli_close($con);
		}

		return $resualt;

	}

	function create_new_user($firstName, $lastName, $eMail, $password){
		global $table_manager;
		//set the strin with values to INSERT query
		$values = '"'. $firstName .'", "'. $lastName .'", "'. $eMail .'", "'. $password .'"';
		

		$query = "INSERT INTO $table_manager->table_name (". implode(', ', $table_manager->fields) .") VALUES (\"\", $values)";

		$exe = exeQuery( $query );

		return $exe;
	}

	function checkLoginDetails($email, $password){

		global $table_manager;

		$exe = exeQuery( "SELECT * FROM $table_manager->table_name WHERE eMail='$email' AND password=$password" );

		return $exe;
	}

	function inviteUser( $val, $isManage = false, $data = '', $DAL=null ){

		global $_SESSION;
		
		if(!isset($DAL)){
			$DAL = new DAL();
		}

		$name = substr( $val, 0, strrpos( $val, '@' ) );

		$newUser = new stdClass();
		$newUser->firstName = $name;
		$newUser->eMail = $val;
		$newUser->password = ($isManage)? $_GET['password'] : rand( 1000, 9999 );

		if(!isset($newUser->password) || !$newUser->password){return false;}

		// check if user exist
		$results = $DAL->select( "SELECT * FROM elect_it_users WHERE eMail='". $val ."'" );
		if( $results ){ 
			$results = mysqli_fetch_assoc( $results );
			$userId = $results['ID'];
			$row = $results;
		}else{
			$result = $DAL->insert( 'elect_it_users', $newUser );
			
			$results = $DAL->select( "SELECT * FROM elect_it_users WHERE eMail='". $val ."'" );
			$row = mysqli_fetch_assoc( $results );
			$userId = $row['ID'];
		}

		if( ! $userId){ 
			return false;
		}

		if(!$isManage){
			// register user to election
			$userToElection = new stdClass();
			$userToElection->electionID =  $data->electionID;
			$userToElection->userID =  $userId;
			$userToElection->manageApprove =  'true';

			$result = $DAL->select( "SELECT * FROM elect_it_election_to_users WHERE electionID='". $userToElection->electionID ."' AND userID='". $userToElection->userID ."'" );
			if( !$result ){ 
				$result = $DAL->insert( 'elect_it_election_to_users', $userToElection );
			}
			// send registration email
			$getElectionResults = $DAL->select('SELECT * FROM elect_it_election WHERE ID='. $data->electionID );
			if( ! $getElectionResults){ 
				return false;
			}
			$getElectionRow = mysqli_fetch_assoc($getElectionResults);
		}

		$mailData = array(
			'address'=> $row['eMail'],
			'name'=> $row['firstName'],
			'subject'=> 'הזמנה להצביע באמצעות Elect-It',
			'template'=> 'templates/invitation.php',
			'content'=> array(
				'name'=> $row['firstName'],
				'manageName'=> $_SESSION['firstName'].' '.$_SESSION['lastName'],
				'electionName'=> (isset($getElectionRow))? $getElectionRow['electionName']: '',
				'address'=> $row['eMail'],
				'password'=> $row['password']
				)
			);
		require_once('../util/send-mail.php');
		$mailResults = sendMali( $mailData );
		if( ! $mailResults->status ){
			return false;
		}
		return true;
	}