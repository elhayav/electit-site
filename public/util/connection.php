<?php 
/*********************************************
	Files Includes
**********************************************/
	require_once 'db_classes.php';
	
/*********************************************
	Decliration on data base tables
**********************************************/
	$table_contestant = new db_table('elect_it_contestant', array('ID', 'fullName', 'description', 'votesCount', 'contestantImage', 'election' , 'contestantImage'));
	$table_election = new db_table('elect_it_election', array('ID', 'manager', 'electionName', 'description', 'electionImage', 'endElectionDate'));
	$table_election_to_users = new db_table('elect_it_election_to_users', array('ID', 'electionID', 'userID', 'voteTo'));
	$table_manager = new db_table('elect_it_manager', array('ID', 'firstName', 'lastName', 'eMail', 'password'));
	$table_users = new db_table('elect_it_users', array('ID', 'firstName', 'lastName', 'phone', 'eMail', 'password', 'userProfileImg', 'voteTo'));

/*********************************************
	Decliration on data base variabels
**********************************************/
	$db_host = "50.62.209.78:3306";
	$db_user_name = "electit_manage";
	$db_password = "Rfcu059";
	$db_data_base_name = "electit_database";
	$tables_names_array = array($table_contestant->table_name,
									$table_election->table_name,
									$table_election_to_users->table_name,
									$table_manager->table_name,
									$table_users->table_name);

	$electit_database = new db_variables($db_host, $db_user_name, $db_password, $db_data_base_name, $tables_names_array);
	
	
	
	function connect_to_db()
	{
		global $electit_database;
		$link = mysqli_connect($electit_database->connection_array[0], $electit_database->connection_array[1], $electit_database->connection_array[2], $electit_database->connection_array[3]);
		return $link;
	}

 ?>