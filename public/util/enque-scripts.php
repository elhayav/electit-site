<?php 
	/* control om scripts */

	
	class EnqueScript{

		private static $scripts = array();
		private static $libs = array();

		public function getScripts(){
			$html = '';

			foreach(self::$libs as $key => $val){

				$html .= '<script type="text/javascript" src="js/libs/'. $val .'.min.js"></script>';

			}
			foreach(self::$scripts as $key => $val){

				$html .= '<script type="text/javascript" src="js/'. $val .'.js"></script>';

			}

			return $html;
		}
		public function enque($add=array()){
			
			foreach( $add as $key => $val){

				if ( $key === 'libs'){ self::libs( $val ); }
				else{

					if( ! in_array( $val, self::$scripts) ){ self::$scripts[] = $val; }
				}
			}
		}
		protected function libs( $lib=array() ){

			foreach( $lib as $key => $val){

				if( ! in_array( $val, self::$libs) ){ self::$libs[] = $val; }
				
			}
		}
	}

	class EnqueStyle{

		private static $styles = array();
		private static $libs = array();

		public function getStyles(){
			$html = '';

			foreach( self::$libs as $key => $val){

				$html .= '<link rel="stylesheet" href="css/libs/'. $val .'.min.css">';

			}
			foreach(self::$styles as $key => $val){

				$html .= '<link rel="stylesheet" href="css/'. $val .'.css">';

			}

			return $html;
		}
		public function enque($add=array()){
			
			foreach( $add as $key => $val){

				if( $key === 'libs'){ self::libs( $val ); }
				else{

					if( ! in_array( $val, self::$styles) ){ self::$styles[] = $val; }
				}
			}
		}
		protected function libs( $lib=array() ){

			foreach( $lib as $key => $val){

				if( ! in_array( $val, self::$libs) ){ self::$libs[] = $val; }
				
			}
		}
	}

	class EnqueMessage{

		private static $message = array();

		public function getMessage(){

			foreach(self::$message as $key => $val){

				include( $val . '.html');

			}
		}
		public function enque($add=array()){
			
			foreach( $add as $key => $val){

					if( ! in_array( $val, self::$message) ){ self::$message[] = $val; }
			}
		}
	}

	$_resurse = new stdClass();
	$_resurse->scripts = new EnqueScript();
	$_resurse->styles = new EnqueStyle();
	$_resurse->messages = new EnqueMessage();

	function getHeader(){

		global $_resurse;

		echo $_resurse->styles->getStyles();
	}
	function getFooter(){

		global $_resurse;

		$_resurse->messages->getMessage();
		echo $_resurse->scripts->getScripts();
	}

 ?>