<?php 
/*********************************************
	Files Includes
**********************************************/
	require_once 'db_classes.php';
	
/*********************************************
	Decliration on data base tables
**********************************************/
	$table_contestant = new db_table('elect_it_contestant', array('ID', 'fullName', 'description', 'votesCount', 'contestantImage', 'election' , 'contestantImage'));
	$table_election = new db_table('elect_it_election', array('ID', 'manager', 'electionName', 'description', 'electionImage', 'endElectionDate'));
	$table_election_to_users = new db_table('elect_it_election_to_users', array('ID', 'electionID', 'userID', 'voteTo'));
	$table_manager = new db_table('elect_it_manager', array('ID', 'firstName', 'lastName', 'eMail', 'password'));
	$table_users = new db_table('elect_it_users', array('ID', 'firstName', 'lastName', 'phone', 'eMail', 'password', 'userProfileImg', 'voteTo'));

/*********************************************
	Decliration on data base variabels
**********************************************/
	$db_host = "50.62.209.78:3306";
	$db_user_name = "electit_manage";
	$db_password = "Rfcu059";
	$db_data_base_name = "electit_database";
	$tables_names_array = array($table_contestant->table_name,
									$table_election->table_name,
									$table_election_to_users->table_name,
									$table_manager->table_name,
									$table_users->table_name);

	$electit_database = new db_variables($db_host, $db_user_name, $db_password, $db_data_base_name, $tables_names_array);
	
	
	
	function connect_to_db()
	{
		global $electit_database;
		$link = mysqli_connect($electit_database->connection_array[0], $electit_database->connection_array[1], $electit_database->connection_array[2], $electit_database->connection_array[3]);
		return $link;
	}

	/**
	* 
	*/
	class DAL{
		
		public static $isError = false;
		private static $link;
		
		function __construct(){
			
			global $electit_database;

			if(isset(self::$link)){ return;}
			
			self::$link = mysqli_connect(  	$electit_database->connection_array[0], 
											$electit_database->connection_array[1], 
											$electit_database->connection_array[2], 
											$electit_database->connection_array[3]  );//connection link

			if( mysqli_connect_errno( self::$link ) ){

				//ERROR message when connection failed
				echo("Failed to connect to MySQL: " . mysqli_connect_error());

			}else{ mysqli_set_charset( self::$link, "utf8" ); }

		}

		function __destruct(){ mysqli_close( self::$link ); }

		public function getConnection(){
			return self::$link;
		}

		public function insert( $table='', $values='' ){
			$column = '';
			$columnData = '';
			$isfirstInValues = true;

			if( ! $table || ! $values ){
				return null;
			}

			foreach($values as $key => $val) {
				

				$column .= ( $isfirstInValues )? '': ', ';
				$columnData .= ( $isfirstInValues )? '': ', ';

				$isfirstInValues = false;
				
				$column .= $key ;
				$columnData .= '"'. $val .'"';
			}
			
			$query = 'INSERT INTO '. $table . ' ('. $column .') VALUES ('. $columnData .')' ;
			$results = mysqli_query( self::getConnection() , $query );
			
			if ($results){

				$query = 'SELECT ID FROM '. $table .' WHERE ID= LAST_INSERT_ID()';

				$results = self::select( $query );

				if( $results ){

					$row = mysqli_fetch_assoc($results);
					$id = $row['ID'];
					return $id;

				}

				return self::printJsonResults(false, '"description": "no id return from db"', 3);
			}
			return self::printJsonResults(false, '"description": "no data return from db"', 3);


		}
		public function select( $query ){

			$results = mysqli_query( self::getConnection() , $query);
			if( $results && $results->num_rows) {
				return $results;
			}
			return null;
		}
		public function update( $table='', $values='', $ID ){

			$columnData = '';
			$isfirstInValues = true;

			if( ! $table || ! $values ){
				return null;
			}

			foreach($values as $key => $val) {
				

				$columnData .= ( $isfirstInValues )? '': ',';
				$isfirstInValues = false;

				$columnData .= '`'. $key .'`="'. $val .'"' ;
			}
			
			$query = 'UPDATE '. $table . ' SET '. $columnData .' WHERE ID='. $ID  ;
			$results = mysqli_query( self::getConnection() , $query );

			if ($results){

				return true;
			}
			self::printJsonResults(false, '"description": "Update failed"', 2);


		}
		public function delete( $table='', $ID ){

			$query = 'DELETE FROM '. $table .' WHERE ID='. $ID ;

			$results = mysqli_query( self::getConnection(), $query);
			if( $results ) {
				return $results;
			}
			return null;

		}
		public function deleteFile( $path, $id, $column, $table ){

			$query = "SELECT `".$column."` FROM $table WHERE ID='".$id."'";
			// print_r($query);
			// print_r(self::$link);
			$result = mysqli_query( self::$link , $query );
			$row = mysqli_fetch_array($result);

			if ($row[$column]) {
				$path = 'images/'. $path .'/';
				$file = $path.$row[$column];
				$thumbnail = $path.'thumbnail/'.$row[$column];
				// Delete Files
				if( file_exists($file)      ){ unlink($file);      }
				if( file_exists($thumbnail) ){ unlink($thumbnail); }

				$query = "UPDATE $table SET `electionImage`='' WHERE ID='".$id."'";
				$result = mysqli_query( self::getConnection() , $query );
				
				if ($result >= 1) {
					return true;
				}else{
					return false;
				}
			}else{
				return true;
			}
		}
		public function printJsonResults( $status=false, $data='', $errorId=0){

			

			if( ! self::$isError ){

				$json = '{"status" : ';
				$json .= ($status)?'true,': 'false,';
				$json .= '"data" : { '. $data ;
				$json .= ( $status )? '} }': ', "errorId": '. $errorId .' } }';

				echo $json;
			}
			self::$isError = true;
		}
	}

 ?>