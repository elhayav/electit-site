
<?php
	require_once('../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php');

	function sendMali( $data ){

		$sendResults = new stdClass();

		if( !isset( $data ) ){
			$sendResults->status = false;
			$sendResults->error =  'Missing data object';
			return $sendResults;
		}
////////////////////////////
		//Create a new PHPMailer instance
		$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
		$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
		$mail->SMTPDebug = 0;

		$mail->CharSet = 'UTF-8';

//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';
//Set the hostname of the mail server
//		$mail->Host = 'smtpout.secureserver.net';
// use
 $mail->Host = gethostbyname('smtpout.secureserver.net');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
		$mail->Port = 3535;
//Set the encryption system to use - ssl (deprecated) or tls
//		$mail->SMTPSecure = 'ssl';
//Whether to use SMTP authentication
		$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
		$mail->Username = "info@elhay.co.il";
//Password to use for SMTP authentication
		$mail->Password = "132qew!@";
//Set who the message is to be sent from
		$mail->setFrom('info@elhay.co.il', 'Elect-It admin site');
//Set an alternative reply-to address
		$mail->addReplyTo('info@elhay.co.il', 'Elect-It');
//Set who the message is to be sent to
		$mail->addAddress($data['address'], $data['name']);
//Set the subject line
		$mail->Subject = $data['subject'];
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
		global $mail_data;
		$mail_data = $data['content'];
		$mail_message_string = get_include_contents($data['template'], $data['content']);

		$mail->msgHTML($mail_message_string, dirname(__FILE__));
//Replace the plain text body with one created manually
		$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
		$mail->addAttachment('images/phpmailer_mini.png');
//send the message, check for errors
		if (!$mail->send()) {
			$sendResults->status = false;
			$sendResults->error =  $mail->ErrorInfo;
			return $sendResults;
		} else {
			$sendResults->status = true;
			return $sendResults;
		}
	}

	function get_include_contents($filename, $variablesToMakeLocal) {

		extract($variablesToMakeLocal);
		$filename = dirname(__FILE__).'/'.$filename;

		if (is_file($filename)) {
			ob_start();
			include $filename;
			return ob_get_clean();
		}
		return false;
	}

