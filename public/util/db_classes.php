<?php 
	class db_variables{
		var  $db_host;
		var  $db_user_name;
		var  $db_password;
		var  $db_data_base_name;

		public  $tables;
		var  $connection_array;

		function db_variables($db_host, $db_user_name, $db_password, $db_data_base_name, $tables_names_array = array()){
			$this->db_host = $db_host;
			$this->db_user_name = $db_user_name;
			$this->db_password = $db_password;
			$this->db_data_base_name = $db_data_base_name;

			$this->tables = $tables_names_array;

			$this->connection_array = array($this->db_host, $this->db_user_name, $this->db_password, $this->db_data_base_name);
		}
	}

	class db_table{
		var $table_name;
		var $fields;

		function db_table($table_name, $fields_array = array())
		{
			$this->table_name = $table_name;
			$this->fields = $fields_array;
		}
	}
 ?>