var ElectIt = ( ElectIt )? ElectIt: {};

ElectIt.public = {
	init: function(){
		/* Login */
		// formOpen
		$( 'a.login-form' ).click( function(){
			ElectIt.public.login.formOpen();
		});
		// formSend
		$('#loginForm').submit( function(e){
			ElectIt.public.login.formSend(e);
		} );

		/* Registration */
		// formOpen
		$( 'a.registration-form' ).click( function(){
			ElectIt.public.registration.formOpen();
		});
		// formSend
		$('#registrationForm').submit( function(e){
			ElectIt.public.registration.formSend(e);
		} );
	},
	login:{
		formOpen: function(){
			$( '#loginModal' ).modal('show');
			$("#loginForm").validate({
				rules: {
					password:{ required: true },
					email:{ required: true }
				}
			});
		},
		formSend: function(e){
			e.preventDefault()
			var form = $("#loginForm");
			form.validate();

			if(form.valid()){
				ElectIt.loading( true );
				var url = 'api/login.php';

				$.ajax({

					type: "GET",
					url: url,
		          	data: $("#loginForm").serialize(), // serializes the form's elements.
		          	success: ElectIt.public.login.checkResults.serverSuccess,
		          	error: ElectIt.public.login.checkResults.serverError

		          });	

			}
		},
		checkResults: {
			serverSuccess: function( data ){

				var message = $('#loginModal');
				if(data.login){

					location.replace("index.php?page=dashboard");
				}
				else{
					ElectIt.loading( false );
					$( message ).find('.modal-body .erorr').html('<p>Wrong email or password</p>')
					.css('background-color', 'red');
				}
			},
			serverError: function( data ){

				ElectIt.loading( false );
				console.log(data);
			}
		}
	},
	registration:{
		formOpen: function(){
			$( '#registrationModal' ).modal('show');
			$("#registrationForm").validate({
				rules: {
					password: "required",
					confirm_password: {
						equalTo: '#registrationForm #password'
					}
				}
			});
					// confirm_password: { equalTo: "password" }
		},
		formSend: function(e){
			e.preventDefault()
			var form = $("#registrationForm");
			form.validate();

			if(form.valid()){

				ElectIt.loading( true );
				var url = 'api/registration.php';

				$.ajax({

					type: "GET",
					url: url,
		           data: $("#registrationForm").serialize(), // serializes the form's elements.
		           success: ElectIt.public.registration.checkResults.serverSuccess,
		           error: ElectIt.public.registration.checkResults.serverError

		       });	
			}
		},
		checkResults: {
			serverSuccess: function( data ){

				ElectIt.loading( false );
				var message = $('#registrationModal');

				if(data.success){

					$( message ).find('.modal-body').html('<h3>Registration successfully completed</h3>')
					.css('background-color', 'green');
					$( message ).find('button.form-send').remove();
				}
				else{
					$( message ).find('.modal-body').html('<h3>Registration Faild ...</h3><p>try another email address </p>')
					.css('background-color', 'gray');
					$( message ).find('button.form-send').remove();
				}
			},
			serverError: function( data ){

				ElectIt.loading( false );
				console.log( data );
			}
		}
	}
}

ElectIt.public.init();