var ElectIt = ( ElectIt )? ElectIt: {};

ElectIt.electionView = {

	init: function(){

		// init click events
			
		$('#edit-contestant-form').submit( function(e){

			e.preventDefault();
			ElectIt.electionView.update();
		} );

		$( '#delete-contestant' ).click( function( e ){

			var id = $( '#deleteContestantModel #hiddenContestantID' ).val(),
				electionId = $( '#deleteContestantModel #hiddenelectionID' ).val();
			
			ElectIt.electionView.delete( id, electionId );

		});
		$("#edit-contestant-form").validate({
			rules: {
				fullName:{ required: true },
				description:{ required: true }
			}
		});

		// image upload init
		ElectIt.electionView.uploadImage.init();
	},
	contestantList: {
		contestant: null,
		showEditContestantModal: function( id, electionId ){

			var fillContestantModal= function( contestant ){

				$( '#editContestantModel #hiddenContestantID' ).val( contestant.ID );
				$( '#editContestantModel #contestantName' ).val( contestant.fullName );
				$( '#editContestantModel #description' ).val( contestant.description );
				$('#editContestantModel').modal('show');

			};

			if( ElectIt.electionView.contestantList.contestant && ElectIt.electionView.contestantList.contestant.ID == id ){
				fillContestantModal( ElectIt.electionView.contestantList.contestant );
				
			}else{

				ElectIt.electionView.get( id, function( data ){

					ElectIt.loading( false );
					if ($.isPlainObject(data) && data.status){

						ElectIt.electionView.contestantList.contestant = data.data;
						fillContestantModal( data.data );

					}else{
						// TODO error message
						console.log('some error');
					}
				} );
			}
		},
		showDeleteContestantModal: function( id, electionId ){
			$( '#deleteContestantModel #hiddenContestantID' ).val( id );
			$( '#deleteContestantModel #hiddenelectionID' ).val( electionId );
			$('#deleteContestantModel').modal('show');
		}
	},
	voters: {
		approved: function( ID ){

			ElectIt.loading( true );
			var url = 'api/election.php';
			data = { action: 'approved', ID: ID };

			$.ajax({

				type: "POST",
				url: url,
				data: data,
				success: function(data){
					if($.isPlainObject(data) && data.status){

						document.location.reload(true);
					}else{
						ElectIt.loading( false );
						console.log( 'Error: ',data );
					}
				},
				error: function(data){
					ElectIt.loading( false );
					console.log('error', data);
				}

			});

		},
		remove: function( ID ){
			ElectIt.loading( true );
			var url = 'api/election.php';
			data = { action: 'cancelApproved', ID: ID };

			$.ajax({

				type: "POST",
				url: url,
				data: data,
				success: function(data){
					if($.isPlainObject(data) && data.status){

						document.location.reload(true);
					}else{
						ElectIt.loading( false );
						console.log( 'Error: ',data );
					}
				},
				error: function(data){
					ElectIt.loading( false );
					console.log('error', data);
				}

			});
		}
	},
	addContestants: function( electionId ){
		ElectIt.electionView.create( ElectIt.dashboard.newElectionForm.data.contestants , electionId );
	},
	create: function( contestantArray, electionId ){

		if( contestantArray ){

				ElectIt.loading( true );
				// send forms
				var url = 'api/contestant.php';
				data = { contestants: contestantArray, election: electionId, action: 'new' };

				$.ajax({

					type: "POST",
					url: url,
					data: data, 
					success: function( data ){
						document.location.reload(true);
					},
					error: function( data ){
						ElectIt.loading( false );
						console.log('error', data);
					}

				});
			}else{
				// form not valid
			}

	},
	update: function(  ){		

		var contestant = {};
		var edit_form = $("#edit-contestant-form");
		edit_form.validate();

		if(edit_form.valid()){
			$.each($('#edit-contestant-form').serializeArray(), function(i, field) {
			    contestant[field.name] = field.value;
			});

			ElectIt.electionView.set( contestant, function( data ){

				ElectIt.loading( false );
				if($.isPlainObject(data) && data.status){

					document.location.reload(true);

				}else{
					// TODO eror message
					console.log( data );
				}
			} );
		}
	},
	set: function( contestant, success ){

		ElectIt.loading( true );
		var url = 'api/contestant.php';
			data = { action: 'set', contestant: contestant };

			$.ajax({

				type: "POST",
				url: url,
				data: data,
				success: success,
				error: function(data){
					ElectIt.loading( false );
					console.log('error', data);
				}

			});
	},
	get: function( id, success ){

		ElectIt.loading( true );
		var url = 'api/contestant.php';
		data = { action: 'get', ID: id };

		$.ajax({

			type: "POST",
			url: url,
			data: data,
			success: success,
			error: function(data){
				ElectIt.loading( false );
				console.log('error', data);
			}

		});

	},
	uploadImage: {
		init: function(){
			$('#fileupload-election').fileupload({
				dataType: 'json',
				formData: { 
					action: 'picture',
					file_name: $('#fileupload-election').attr( 'election-id' )
				},
		        done: function (e, data) {
		            $.each(data.result.files, function (index, file) {
		            	$( '.election-details img.img-thumbnail' ).attr( 'src', file.thumbnailUrl );
		            });
		        }
		    });
		    $.each( $( '.fileupload-contestant' ), function( key, val ){ 
		    	var contestantId = $( val ).attr( 'contestant-id' );
		    	
				$( val ).fileupload({
					dataType: 'json',
					formData: { 
						action: 'picture',
						file_name: contestantId
					},
			        done: function (e, data) {
			            $.each(data.result.files, function (index, file) {
			            	$('img[contestant-id="'+contestantId+'"]').attr( 'src', file.thumbnailUrl + '?' + Math.random() );
			            	console.log($('img[contestant-id="'+contestantId+'"]'));
			            });
			        }
				});
		    } );
		}
	},
	delete: function( id, electionId ){

		ElectIt.loading( true );

		var url = 'api/contestant.php';
		data = { action: 'delete', ID: id, election: electionId };

		$.ajax({

			type: "POST",
			url: url,
			data: data,
			success: function( data ){
				
				if($.isPlainObject(data) && data.status){

					document.location.reload(true);
				}else{
					ElectIt.loading( false );
					console.log( 'Error: ',data );
				}					
			},
			error: function(data){
				ElectIt.loading( false );
				console.log('Error: ', data);
			}

		});
	}
}

ElectIt.electionView.init();