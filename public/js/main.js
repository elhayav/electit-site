var ElectIt = {

	events: {
		init: function(){
			// About -> link click
			$( 'a.about-php' ).click( function(){
				$( '#aboutModel' ).modal('show');
			}); 

			// Contact as -> llick
			$( 'a.contactUs' ).click( function(){
				$( '#ContactUsModel' ).modal('show');
			}); 

		}
			
	},
	loading: function( isVizable ){

		if (isVizable){

			$('#loading').show();
		}else{
			$( '#loading' ).hide();
		}
	},
	election: {

		set: function( election, success ){

			ElectIt.loading( true );
			var url = 'api/election.php';
				data = { action: 'setElection', election: election };

				$.ajax({

					type: "POST",
					url: url,
					data: data,
					success: success,
					error: function(data){
						ElectIt.loading( false );
						console.log('error', data);
					}

				});

		},
		get: function( id, success ){

			ElectIt.loading( true );
			var url = 'api/election.php';
			data = { action: 'getElection', ID: id };

			$.ajax({

				type: "POST",
				url: url,
				data: data,
				success: success,
				error: function(data){
					ElectIt.loading( false );
					console.log('error', data);
				}

			});

		},
		delete: function( id ){

			ElectIt.loading( true );

			var url = 'api/election.php';
			data = { action: 'deleteElection', ID: id };

			$.ajax({

				type: "POST",
				url: url,
				data: data,
				success: function( data ){
					
					if($.isPlainObject(data) && data.status){

						document.location.replace('/');
					}else{
						ElectIt.loading( false );
						console.log( 'Error: ',data );
					}					
				},
				error: function(data){
					ElectIt.loading( false );
					console.log('Error: ', data);
				}

			});
		}
	},
	init: function() {

			// Evets leseners init
			ElectIt.events.init();
	},
	alert: function( content, type ){

		var template = '<div class="alert alert-'+ type +' alert-dismissable">';
			template += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
			template += content;
			template += '<div>';
		$( '#alert' ).append( template );

	}
};

ElectIt.loading( true );
ElectIt.init();
$( function(){
	ElectIt.loading( false );
});
