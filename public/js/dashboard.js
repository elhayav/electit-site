var ElectIt = ( ElectIt )? ElectIt: {};

ElectIt.dashboard = {

	init: function(){

		ElectIt.dashboard.newElectionForm.init();
		ElectIt.dashboard.electionList.init();
		ElectIt.dashboard.user.init();
		ElectIt.dashboard.invitations.init();
	},
	newElectionForm: {

		init: function(){

			// next button click
			$( '#new-election-send' ).click( ElectIt.dashboard.newElectionForm.next );
			$( '#add-contestant' ).click( ElectIt.dashboard.newElectionForm.addContestant );

			$('#election-form-tabs a:first').tab('show');

			$( '#new-election-step-1' ).submit( function(e){
				e.preventDefault();
			});
			$( '#new-election-step-2' ).submit( function(e){
				e.preventDefault();
			});
			
			// Validation rules
			$("#new-election-step-1").validate({
				rules: {
					electionName:{ required: true },
					description:{ required: true },
					endElectionDate:{ required: true }
				}
			});
			$("#new-election-step-2").validate({
				rules: {
					fullName:{ required: true },
					description:{ required: true }
				}
			});

			$('#election-form-tabs a').on('shown.bs.tab', function (e) {
			  e.preventDefault();
			  
			  if(  $(this).attr('href') === '#step-2' ){

			  	// check forom 1 validation befor open tab 2
			  	if( ElectIt.dashboard.newElectionForm.status ){
			  		$( e.target ).tab('show');	
			  	}else{
			  		
			  		$( e.relatedTarget  ).tab('show');	
			  	}

			  }else{ $( e.target ).tab('show'); }
			})

			// Date picker
			$('#election-datetimepicker').datetimepicker({
				maskInput: false, 
				pickDate: true,
				pickTime: true,
				pickSeconds: false,
				startDate: new Date(),
				language: 'en',
				format: 'yyyy-MM-dd hh:mm'
			});

		},
		formStatus: 0,
		data: {
			election: {},
			contestants: []
		},
		sendAll: function( data ){

			// TODO forms validation
			if( data ){

				ElectIt.loading( true );
				// send forms
				var url = 'api/election.php';
				data.action = 'newElection';
				
				$.ajax({

					type: "POST",
					url: url,
					data: data, // serializes the form's elements.
					success: function(data){
						document.location.replace('?page=election&electionID=' + data.data.electionID);
					},
					error: function(data){
						ElectIt.loading( false );
						console.log('error', data);
					}

				});
				// check the results

					// add reload the page

					// error message
			}else{
				// form not valid
			}
		},
		next: function(){

			var step_1_form = $("#new-election-step-1");			

			if( ! ElectIt.dashboard.newElectionForm.status ){

				// valid election form
				step_1_form.validate();

				if(step_1_form.valid()){

					ElectIt.dashboard.newElectionForm.status = 1;
					// stor election form data in ElectIt.dashboard.newElectionForm.data.election
					$.each($('#new-election-step-1').serializeArray(), function(i, field) {
					    ElectIt.dashboard.newElectionForm.data.election[field.name] = field.value;
					});
					// enable tab 2 and go to tab 2
					$('#election-form-tabs a:last').tab('show');
					
				}

			}else{

				step_1_form.validate();

				if ( ElectIt.dashboard.newElectionForm.data.contestants.length > 1) {
					if(step_1_form.valid()){
						// clear forms
						$('#new-election-step-1')[0].reset();
						$('#new-election-step-2')[0].reset();

						// send the form
						ElectIt.dashboard.newElectionForm.sendAll( ElectIt.dashboard.newElectionForm.data )
					}else{
						ElectIt.dashboard.newElectionForm.status = 0;
						$('#election-form-tabs a:first').tab('show');
					}
				}else{

					ElectIt.alert('Required too contestant..', 'warning');
				}
			}
		},
		addContestant: function(){

			var step_2_form = $("#new-election-step-2");
			step_2_form.validate();

			if(step_2_form.valid()){

				// store the data in contestant array
				var values = {};
				var $inputs = $('#new-election-step-2 :input');
			    $inputs.each(function() {
			        values[this.name] = $(this).val();
			    });

			    ElectIt.dashboard.newElectionForm.data.contestants.push(values);
				// show contestant name in list
			    $('.tmep-contestants').append('<li id="'+ ElectIt.dashboard.newElectionForm.data.contestants.length +'"><span class="label label-default"><i class="fa fa-minus-square-o fa-1"></i><strong>   1: </strong>'+ values.fullName +'</span></li>');
			   
				// clear the form
				$('#new-election-step-2')[0].reset();
			}
		}
	},
	electionList: {
		init: function(){

			// Validation roles
			$("#edit-election-form").validate({
				rules: {
					electionName:{ required: true },
					description:{ required: true },
					endElectionDate:{ required: true }
				}
			});
			// Date picker
			$('#editElectionModel #election-datetimepicker').datetimepicker({
				maskInput: false, 
				pickDate: true,
				pickTime: true,
				pickSeconds: false,
				startDate: new Date(),
				language: 'en',
				format: 'yyyy-MM-dd hh:mm'
			});
			// init Edit election buttons
			$('a[action="edit"]').click( function( e ){

				id = $( e.target ).attr('election-id');
				ElectIt.dashboard.electionList.showEditElectionModal( id );

				$('#edit-election-form').submit( ElectIt.dashboard.electionList.update );
			} );

			// init Delete election buttons
			$('a[action="delete"]').click( function( e ){

				var id = $( e.target ).attr( 'election-id' );
				$('#deleteElectionModel').modal('show');
				$( '#deleteElectionModel #hiddenElectionID' ).val( id );
			} );
			$( '#delete-election' ).click( function( e ){

				var id = $( '#deleteElectionModel #hiddenElectionID' ).val();
				
				ElectIt.election.delete( id );

			});
		},
		election: null,
		showEditElectionModal: function( id ){
			
			var fillElectionModal= function( election ){

					$( '#editElectionModel #hiddenElectionID' ).val( election.ID );
					$( '#editElectionModel #electionName' ).val( election.electionName );
					$( '#editElectionModel #description' ).val( election.description );
					$( '#editElectionModel #endElectionDate' ).val( election.endElectionDate );
					$('#editElectionModel').modal('show');

			};
			if( ElectIt.dashboard.electionList.election && ElectIt.dashboard.electionList.election.ID == id ){
				fillElectionModal( ElectIt.dashboard.electionList.election );
			}else{

				ElectIt.election.get( id, function( data ){

					ElectIt.loading( false );
					if ($.isPlainObject(data) && data.status){
					
						election = data.data;

						election.endElectionDate = election.endElectionDate ;
						// election.endElectionDate = new Date(election.endElectionDate );
						
						// day = election.endElectionDate.getDate();
						// month = election.endElectionDate.getMonth()+1;
						// year = election.endElectionDate.getFullYear();

						// month = ( month > 9 )? month: '0'+ month;
						// day = ( day > 9 )? day: '0'+ day;

						// election.endElectionDate = year +'-'+ month +'-'+ day;

						ElectIt.dashboard.electionList.election = election;
						fillElectionModal( election );
						

					}else{
						// TODO error message
					}
				} );
			}
		},
		update: function( e ){

			e.preventDefault();
			var election = {};

			var edit_form = $("#edit-election-form");
			edit_form.validate();

			if(edit_form.valid()){

				$.each($('#edit-election-form').serializeArray(), function(i, field) {
				    election[field.name] = field.value;
				});

				ElectIt.election.set( election, function( data ){

					if($.isPlainObject(data) && data.status){

						document.location.reload(true);

					}else{
						ElectIt.loading( false );
						// TODO eror message
						console.log( data );
					}
				} );
			}
		}
	},
	user: {
		init: function(){

			$('#edit-user-form .user-form').click( ElectIt.dashboard.user.editable );
			$('#edit-user-form').submit( ElectIt.dashboard.user.send );
		},
		editable: function(){

			$('#edit-user-form .user-form').attr( 'contenteditable', 'true');
			$('#edit-button').hide();
			$('#edit-user-form .edit-mode').removeClass( 'hide' );
		},
		cancel: function(){

			$('#edit-user-form .user-form').attr( 'contenteditable', 'false');
			$('#edit-button').show();
			$('#edit-user-form .edit-mode').addClass( 'hide' );
		},
		send: function(e){
			e.preventDefault();
			ElectIt.loading( true );

			var data = {
					action 	 : 'editUser',
					user: {
						firstName: $('#firstName.user-form').text(),
						lastName : $('#lastName.user-form').text(),
						eMail : $('#eMail.user-form').text()
					}
				},
				url = 'api/user.php';

			if( ! ElectIt.dashboard.user.validUserForm( data.user ) ){
				return;
			}


			$.ajax({

				type: "POST",
				url: url,
				data: data,
				success: function( data ){

					if(data.status){
						document.location.reload(true);
					}else{
						// TODO error message...
						console.log(data);
					}
					ElectIt.loading( false );
				},
				error: function(data){
					ElectIt.loading( false );
					console.log('error', data);
				}

			});
		},
		validUserForm: function( user ){

			if( !user ){ return false; }
			if( user.firstName.length < 1 ){ 
				$( '#firstName' ).parent('td').append('<span style="color:red;">*</span>');
				return false; 
			}
			if( user.lastName.length < 1 ){ 
				
				$( '#lastName' ).parent('td').append('<span style="color:red;">*</span>');
				return false; 
			}
			if( user.eMail.length < 1 ){ 
				
				$( '#eMail' ).parent('td').append('<span style="color:red;">*</span>');
				return false; 
			}
			return true;

		},
		showDeleteModal: function(){
			$( '#deleteUserModel' ).modal('show');
		},
		delete: function(){

			ElectIt.loading( true );

			$.ajax({

				type: "POST",
				url: 'api/user.php',
				data: { action: 'deleteUser'},
				success: function( data ){

					if(data.status){
						document.location.reload(true);
					}else{
						// TODO error message...
						console.log(data);
					}
					ElectIt.loading( false );
				},
				error: function(data){
					ElectIt.loading( false );
					ElectIt.alert( 'Error delete failure, tray later..', 'danger' );
				}

			});
		}
	},
	invitations: {
		data: {
			users:[]
		},
		init: function(){
			// Validation rules
			$("#invite-user-form").validate({
				rules: {
					email:{ required: true }
				}
			});
			// click events ..
			$( '#invite-user-form' ).submit( function(e){
				e.preventDefault();
				ElectIt.dashboard.invitations.addUser();
			});
			$( '#invite-users-form' ).submit( function(e){
				e.preventDefault();
				ElectIt.dashboard.invitations.addUsers();
			});
			$( '#invite-users-form textarea' ).keyup( function(e){
				if( e.which == 13 ){
					$( '#invite-users-form' ).submit();
				}
			});
		},
		addUser: function(){

			var form = $("#invite-user-form");
			form.validate();

			if(form.valid()){
				var user = $( '#invite-user-form  #email' ).val();
				$( '#invite-user-form  #email' ).val('');

				ElectIt.dashboard.invitations.data.users.push( user );
				ElectIt.dashboard.invitations.updateUserList( user );
			}
		},
		updateUserList: function( user ){
			var list = $('.temp-users-list'),
				index = ElectIt.dashboard.invitations.data.users.length -1,
				li = '<li>'+user+'<a onclick="ElectIt.dashboard.invitations.removeUser('+index+')"><i class="fa fa-minus-square-o fa-1"></i></a></li>'
			list.append( li );
		},
		addUsers: function(){

			var form = $("#invite-users-form"),
				pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

				var users = $( '#invite-users-form  textarea' ).val();
				$( '#invite-users-form  textarea' ).val('');

				users = users.replace(/ /g,'');
				users = users.replace(/\n/g, '');

				$.each( users.split(','), function( index, val ){

					//valid email
					if( pattern.test( val ) ){

						ElectIt.dashboard.invitations.data.users.push( val );
						ElectIt.dashboard.invitations.updateUserList( val );
					}
				});
		},
		removeUser: function( index ){

			ElectIt.dashboard.invitations.data.users.splice( index, 1 );
			$('.temp-users-list li')[index].remove();
		},
		send: function( electionID ){
			// send user with ajax 
			ElectIt.loading( true );

			var url = 'api/election.php',
				data= {
					action: 'invitation',
					electionID: electionID,
					users: ElectIt.dashboard.invitations.data.users
				};
				
				$.ajax({

					type: "POST",
					url: url,
					data: data, // serializes the form's elements.
					success: function(data){
						ElectIt.alert( 'Your invitation send success!', 'success' );
						ElectIt.loading( false );
					},
					error: function(data){
						ElectIt.loading( false );
						ElectIt.alert( 'Error your invitation dont send, tray later..', 'danger' );

					}

				});
		}
	}
	
}

$( ElectIt.dashboard.init() );