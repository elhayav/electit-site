<?php 
/*********************************************
Page variabels
**********************************************/
$active_home = "active";

include 'public/header.php';
?>


<div class="container">

	<!-- slogan about the app -->
	<!-- Links to download the app -->
	<div class="jumbotron">
		<div class="row">
			<div class="col-md-9">
				<h1>New Android election app</h1>
				<p>With this awesome app you can create a small election system easily. 
					then you can send the "election number" for all people that you want to. 
					and they immediately can vote in a android App.</p>
			</div>
			<div class="col-md-3">
				<img src="img/icon.png" >
			</div>
		</div>
		<p><a href="https://play.google.com/store/apps/details?id=il.ac.ariel.elect_it" target="_blank"><img src="img/GooglePlay.png"></a></p>
		</div>
		<!-- Screenshot carousel  -->
		<div id="carousel-screenshots" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carousel-screenshots" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-screenshots" data-slide-to="1" class=""></li>
				<li data-target="#carousel-screenshots" data-slide-to="2" class=""></li>
				<li data-target="#carousel-screenshots" data-slide-to="3" class=""></li>
				<li data-target="#carousel-screenshots" data-slide-to="4" class=""></li>
				<li data-target="#carousel-screenshots" data-slide-to="5" class=""></li>
				<li data-target="#carousel-screenshots" data-slide-to="6" class=""></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<img src="img/Screenshot-1.png">
					<div class="carousel-caption bs-callout bs-callout-info">
						<h3>Log in / Registration</h3>
						<p>
							If you already registered just type your email en password <br>and click on login button.
						</p>
						<p>
							If you don't have password - click on register button.
						</p>
					</div>
				</div>
				<div class="item">
					<img src="img/Screenshot-2.png">
					<div class="carousel-caption bs-callout bs-callout-info">
						<h3>Registration</h3>
						<p>Here you need to type just your Email and your firs name.</p>
						<p>Then you will get this email address mail with you password.</p>
						<h4>Congratulation !</h4> <p>now you log in with your email en your password.</p>
					</div>
				</div>
				<div class="item">
					<img src="img/Screenshot-5.png">
					<div class="carousel-caption bs-callout bs-callout-info">
						<h3>Home page</h3>
						<p>This page appear Login</p>
						<p>
							At first time you will see a empty page<br>
							bat don't worry click on "Sign to election" button.
						</p>
					</div>
				</div>
				<div class="item">
					<img src="img/Screenshot-3.png">
					<div class="carousel-caption bs-callout bs-callout-info">
						<h3>Sign to election</h3>
						<p>Type the number of election that you wont to sign.</p>
						<p>If you don't have any number ask the election manager.</p>
					</div>
				</div>
				<div class="item">
					<img src="img/Screenshot-4.png">
					<div class="carousel-caption bs-callout bs-callout-info">
						<h3>Profile page</h3>
						<p>Add or edit your personal details.</p>
					</div>
				</div>
				<div class="item">
					<img src="img/Screenshot-6.png">
					<div class="carousel-caption bs-callout bs-callout-info">
						<h3>Election page</h3>
						<p>After you click on some election that you sign to, this page is open.</p>
						<p>
							Here you can:
						</p>
							<p>
								* Vote to contestant.<br>
								* Click on contestant to show his details.<br>
								* Remove this election from your App.<br>
								* Show this election details.<br>
								* Show this election results.<br>
							</p>

					</div>
				</div>
				<div class="item">
					<img src="img/Screenshot-7.png">
					<div class="carousel-caption bs-callout bs-callout-info">
						<h3>Contestant profile</h3>
						<p>Contestant details.</p>
					</div>
				</div>
			</div>
			<a class="left carousel-control" href="#carousel-screenshots" data-slide="prev">
				<i class="fa fa-chevron-left"></i>
			</a>
			<a class="right carousel-control" href="#carousel-screenshots" data-slide="next">
				<i class="fa fa-chevron-right"></i>
			</a>
		</div>
		<!-- Help: description haw to create election an haw to use in app -->
	</div>


