<?php 
	require_once 'util/db_classes.php';
	require_once 'util/dal.php';
	require_once 'util/db_function.php';
	require_once 'util/enque-scripts.php';

	function isLogin(){

		if(isset($_SESSION['userId'])){
			
			if(isset($_REQUEST['logout']) && $_REQUEST['logout'] == 'true') {
				logOut();
				return false;
			}else{
				return true;
			}
		}else{
			return false;
		}
	};

	function logIn($row){

		if ($row) {
			session_start();
				$_SESSION['userId'] = $row['ID'];
				$_SESSION['firstName'] = $row['firstName'];
				$_SESSION['lastName'] = $row['lastName'];
				$_SESSION['eMail'] = $row['eMail'];
				$_SESSION['password'] = $row['password'];
		}
	};

	function logOut(){

		$_SESSION  = array();
		session_unset();
	};
	
