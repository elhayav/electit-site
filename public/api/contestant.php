<?php 

require_once( '../function.php' );
require_once( '../util/dal.php' );

session_start();
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

	// get the query params
$data = json_decode(file_get_contents("php://input"));

if(!$data) {
	if($_REQUEST) {
		$data = json_decode(json_encode($_REQUEST));
	}else if($_POST){
		$data = json_decode(json_encode($_POST));
	}else{
		$data = new stdClass;
		$data->action = 'badAction';
	}
}

	// open connection to db
$DAL = new DAL();

if( isLogin() ){

	if( isset($data->action) ){


		switch( $data->action ){

			case 'new':

			if( isset( $data->contestants, $data->election ) ){
				

				foreach ( $data->contestants as $key => $val) {
					$val->contestantImage = ( isset($val->contestantImage) )? $val->contestantImage: 'default.png';
					$val->election = $data->election;
					$results = $DAL->insert( 'elect_it_contestant', $val );
				}
				$DAL->printJsonResults( true, '' );
			}
			break;

			case 'get':

			if ( isset( $data->ID ) ) {
				$results = $DAL->select('SELECT * FROM elect_it_contestant WHERE ID='. $data->ID );
				if ( $results && $results->num_rows ){
					$results = mysqli_fetch_assoc( $results );
					$json = '"ID" : "'. $results['ID'] .'","fullName": "'. $results['fullName'] .'", "description": "'. $results['description'] .'"';
				}
			}

			$DAL->printJsonResults( true, $json );

			break;

			case 'set':

				if( isset( $data->contestant, $data->contestant->ID ) ){

					$results = $DAL->update( 'elect_it_contestant', $data->contestant, $data->contestant->ID );

					if ($results) {
						$DAL->printJsonResults( true, '' );
					}else{

						$DAL->printJsonResults( false,  '"description": "update faild"', 2 );
					}
				}else{
					$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
				}

				break;

			case 'delete':
				
				if( isset($data->ID) ){

					$results = $DAL->delete( 'elect_it_contestant', $data->ID );
					
					if ($results) {

						$DAL->printJsonResults( true, '' );

					}else{
						$DAL->printJsonResults( false,  '"description": "delete faild"', 2 );

					}
				}else{
					$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
				}

				break;

			case 'picture':
		
				$DAL->deleteFile(  'contestants', $data->file_name, 'contestantImage', 'elect_it_contestant');
				error_reporting(E_ALL | E_STRICT);
				require('UploadHandler.php');
				$upload_handler = new UploadHandler( null, 'contestantImage/' );

				$upload_handler->get( false );
				$file = $upload_handler->FILE;

				$valuesObj = new stdClass();
				$valuesObj->contestantImage = $file->name;
				$valuesObj->ID = $_REQUEST['file_name'];

				$DAL->update( 'elect_it_contestant', $valuesObj, $valuesObj->ID);

				break;

			default:

			$DAL->printJsonResults(false, '"description": "bad action"', 1);
			break;
		}

	}else{
		$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
	}
}else{
	$DAL->printJsonResults( false,  '"description": "Log in requerd"', 4 );
}
?>