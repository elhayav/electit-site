<?php 

require_once( '../function.php' );
require_once( '../util/dal.php' );

session_start();
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

	// get the query params
$data = json_decode(file_get_contents("php://input"));

if(!$data) {
	if($_REQUEST) {
		$data = json_decode(json_encode($_REQUEST));
	}else if($_POST){
		$data = json_decode(json_encode($_POST));
	}else{
		$data = new stdClass;
		$data->action = 'badAction';
	}
}

	// open connection to db
$DAL = new DAL();

if( isLogin() ){

	if( isset($data->action) ){


		switch( $data->action ){

			case 'newElection':

				if( isset( $data->election, $data->contestants ) ){

					$data->election->electionImage = ( isset($data->election->electionImage) )? $data->election->electionImage: 'default.png';
					$data->election->manager = $_SESSION['userId'];
					$electionId = $DAL->insert( 'elect_it_election', $data->election );

					if ( $electionId && $electionId >=0 ) {
						
						foreach ( $data->contestants as $key => $val) {
							$val->contestantImage = ( isset($val->contestantImage) )? $val->contestantImage: 'default.png';
							$val->election = $electionId;
							$results = $DAL->insert( 'elect_it_contestant', $val );
						}
						$DAL->printJsonResults( true, '"electionID" : '. $electionId  );
					}
				}
				break;

			case 'getElection':

				if ( isset( $data->ID ) ) {
					$results = $DAL->select('SELECT * FROM elect_it_election WHERE ID='. $data->ID );
					if ( $results && $results->num_rows ){
						$results = mysqli_fetch_assoc( $results );
						$json = '"ID" : "'. $results['ID'] .'","electionName": "'. $results['electionName'] .'", "description": "'. $results['description'] .'", "endElectionDate": "'. $results['endElectionDate'] .'"';
					}
				}

				$DAL->printJsonResults( true, $json );

				break;

			case 'setElection':

				if( isset($data->election, $data->election->ID) ){

					$results = $DAL->update( 'elect_it_election', $data->election, $data->election->ID );

					if ($results) {
						$DAL->printJsonResults( true, '' );
					}else{

						$DAL->printJsonResults( false,  '"description": "update faild"', 2 );
					}
				}else{
					$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
				}

				break;

			case 'deleteElection':
				
				if( isset($data->ID) ){

					$results = $DAL->delete( 'elect_it_election', $data->ID );
					
					if ($results) {

						$DAL->printJsonResults( true, '' );

					}else{
						$DAL->printJsonResults( false,  '"description": "delete faild"', 2 );

					}
				}else{
					$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
				}

				break;

			case 'approved':
				if( isset( $data->ID ) ){
					$results = exeQuery( 'UPDATE elect_it_election_to_users SET  manageApprove= "true" WHERE  ID='.$data->ID);

					if ($results) {
						$DAL->printJsonResults( true, '' );
					}else{

						$DAL->printJsonResults( false,  '"description": "update faild"', 2 );
					}
				}else{
					$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
				}
				break;

			case 'cancelApproved':
				if( isset( $data->ID ) ){
					$results = exeQuery( 'UPDATE elect_it_election_to_users SET  manageApprove= "false" WHERE  ID='.$data->ID);

					if ($results) {
						$DAL->printJsonResults( true, '' );
					}else{

						$DAL->printJsonResults( false,  '"description": "update faild"', 2 );
					}
				}else{
					$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
				}
				break;

			case 'invitation':

				if ( ! isset($data->users) ||  ! is_array($data->users) || ! isset($data->electionID) ) {
					$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
					return;
				}

				$status = new stdClass();
				$status->success = 0;
				$status->faild = 0;

				// forEach on $data
				foreach ( $data->users as $index => $val ) {
					// register user to app
					$itemStatus = inviteUser($val, false, $data, $DAL);
					if($itemStatus){
						$status->success ++;
					}else{
						$status->faild ++;
					}
				}
				$DAL->printJsonResults( true, '"invitationStatus" : { "success":'.$status->success. ', "faild":'.$status->faild. '}');
				break;

			case 'picture':

				$DAL->deleteFile(  'elections',$data->file_name, 'electionImage', 'elect_it_election');
				error_reporting(E_ALL | E_STRICT);
				require('UploadHandler.php');
				$upload_handler = new UploadHandler( null, 'electionImage/' );

				$upload_handler->get( false );
				$file = $upload_handler->FILE;

				$valuesObj = new stdClass();
				$valuesObj->electionImage = $file->name;
				$valuesObj->ID = $_REQUEST['file_name'];

				$DAL->update( 'elect_it_election', $valuesObj, $valuesObj->ID);

				break;
			
			default:

			$DAL->printJsonResults(false, '"description": "bad action"', 1);
			break;
		}

	}else{
		$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
	}
}else{
	$DAL->printJsonResults( false,  '"description": "Log in requerd"', 4 );
}
?>