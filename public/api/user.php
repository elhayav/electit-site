	<?php 

	require_once( '../function.php' );
	require_once( '../util/dal.php' );

	session_start();
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');

		// get the query params
	$data = json_decode(file_get_contents("php://input"));

	if(!$data) {
		if($_REQUEST) {
			$data = json_decode(json_encode($_REQUEST));
		}else if($_POST){
			$data = json_decode(json_encode($_POST));
		}else{
			$data = new stdClass;
			$data->action = 'badAction';
		}
	}

		// open connection to db
	$DAL = new DAL();

	if( isLogin() ){

		if( isset($data->action) ){


			switch( $data->action ){

				case 'editUser':

						if( isset( $data->user ) ){
							$results = $DAL->update( 'elect_it_manager', $data->user, $_SESSION['userId'] );
							if ($results) {
								$_SESSION['firstName'] = $data->user->firstName;
								$_SESSION['lastName'] = $data->user->lastName;
								$_SESSION['eMail'] = $data->user->eMail;
								$DAL->printJsonResults( true, '' );
							}else{

								$DAL->printJsonResults( false,  '"description": "update faild"', 2 );
							}

						}else{
							$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
						}
						
					break;

				case 'deleteUser':

					if ( isset( $_SESSION['userId'] ) ) {
					
						$results = $DAL->delete( 'elect_it_manager', $_SESSION['userId'] );
					
						if ($results) {
							logOut();
							$DAL->printJsonResults( true, '' );

						}else{
							$DAL->printJsonResults( false,  '"description": "delete faild"', 2 );

						}
					}else{
						$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
					}

					break;

				default:

					$DAL->printJsonResults(false, '"description": "bad action"', 1);
					break;
			}

		}else{
			$DAL->printJsonResults( false,  '"description": "messing arguments"', 1 );
		}
	}else{
		$DAL->printJsonResults( false,  '"description": "Log in requerd"', 4 );
	}
	?>